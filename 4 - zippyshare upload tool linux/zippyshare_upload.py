import logging
import os
import shutil
import argparse
from time import sleep
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
import selenium.webdriver.chrome.service as service

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s.%(msecs)03d: %(message)s', datefmt='%H:%M:%S')

print('[Info] Start chromedriver service.')

try:
    service = service.Service('./chromedriver')
    service.start()
except Exception as Error:
    print(Error)
    exit()

print('[Info] Service started.')

list_download_links = []


def upload_to_zippyshare(filename):
    options = webdriver.ChromeOptions()
    options.add_argument('--disable-extensions')
    options.add_argument('--headless')
    options.add_argument('--disable-gpu')
    options.add_argument('--no-sandbox')

    driver = webdriver.Remote(service.service_url, desired_capabilities=options.to_capabilities())

    driver.get('http://www.zippyshare.com/sites/index_old.jsp')
    element = driver.find_element_by_id('fupload')  # find upload window
    cwd = os.getcwd()
    element.send_keys(cwd + '\\' + filename)  # send path of the filename
    driver.find_element_by_id("private").click()  # tick private checkbox
    actions = ActionChains(driver)
    actions.send_keys(Keys.RETURN)  # start upload
    actions.perform()
    # wait until upload has finished and retrieve download link
    download_link = driver.find_element_by_id('plain-links').text
    driver.close()
    driver.quit()
    return download_link


def loop_through_cwd(tmp_path):
    global list_download_links
    cwd = tmp_path
    print('[Info] Current working directory:', os.path.join(cwd))

    counter_files = 0

    for file in os.listdir(cwd):
        filename = os.fsdecode(file)
        if filename.endswith(".rar"):
            print('[Info] Detected:', os.path.join(filename))
            counter_files += 1

    print('[Info] Found', counter_files, 'files to upload.')

    for file in os.listdir(cwd):
        filename = os.fsdecode(file)
        if filename.endswith(".rar"):
            print('[Info] Uploading:', filename)
            list_download_links.append(upload_to_zippyshare(filename))
            continue

    print('[Info] Download Links:\n')

    for item in list_download_links:
        print(item)


def upload_target(path):

    if os.path.isdir(path):
        print('[Info] Path found', path)
    else:
        print('[Info] Path not found', path)
        exit()

    path_destination = '/tmp/upload'
    print('[Info] Copy all files to:', path_destination)

    try:
        files = os.listdir(path_destination)
        for file in files:
            print('[Info] Copying:', file)
            shutil.copy(path + '/' + file, path_destination)
    except Exception as Error:
        print(Error)
        exit()

    loop_through_cwd(path_destination)

if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument('-s', '--source', required=True,
                    help='Example: home/folder_one/upload_this_file.rar')
    args = vars(ap.parse_args())
    upload_target(args["source"])
