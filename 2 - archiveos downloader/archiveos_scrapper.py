import logging
import requests
from bs4 import BeautifulSoup
import shutil

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s.%(msecs)03d: %(message)s', datefmt='%H:%M:')
headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; rv:31.0) Gecko/20100101 Firefox/31.0'}

urls = [
    'https://archiveos.org/linux/',
    'https://archiveos.org/bsd/',
    'https://archiveos.org/dos/',
    'https://archiveos.org/solaris/',
    'https://archiveos.org/others/'
]

soup = ''

for url in urls:
    print('[Info] Starting:', url)
    try:
        r = requests.get(url, headers=headers, timeout=3)
        soup = BeautifulSoup(r.content, 'html.parser')
    except Exception as Error:
        print(Error)

    for link in soup.find_all('a'):
        if len(link.get('href')) < 22:  # links have to be longer than 22 chars
            continue
        try:
            print('[Info] Scrapping:', link.get('href'))
            r = requests.get(link.get('href'), headers=headers, timeout=3)
            soup = BeautifulSoup(r.content, 'html.parser')
        except Exception as Error:
            print(Error)

        for item in soup.find_all('a'):
            try:
                if 'sourceforge.net' in item.get('href'):
                    url = item.get('href')
                    print('[Info] Found:', url)
                    name = str(item.get('href')).split('/')[-2]
                    print('[Info] Downloading:', name)
                    response = requests.get(item.get('href'), stream=True)
                    with open(name, 'wb') as name:
                        shutil.copyfileobj(response.raw, name)
                    print('[Info]', name, 'saved.')

            except Exception as Error:
                print(Error)

print('[Info] All downloads finished.')
