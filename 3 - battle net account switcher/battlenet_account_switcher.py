import pyautogui
import logging
import configparser
from tkinter import *
import os
import webbrowser
import psutil
from time import sleep

VERSION = '1.0.4'
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s.%(msecs)03d: %(message)s', datefmt='%H:%M:%S')
p = pyautogui  # lesser visual noise
root = Tk()

before_login_kill_hs = True
after_login_start_hs = True


def initialize_battle_net_path():
    """
    ask user for battlenet path and save it
    """
    root.withdraw()  # don't show tk window to user
    file_path = root.filedialog.askopenfilename(initialdir='/', title='Select Battle.net Launcher.exe')
    config = configparser.ConfigParser()
    config['settings'] = {}
    config['settings']['battle_net_exe_path'] = file_path
    with open('settings.ini', 'w') as configfile:
        config.write(configfile)
    logging.debug(file_path)


def initialize_accounts_from_config():
    """
    get all accounts from accounts.ini
    """
    config = configparser.ConfigParser()
    config.read('accounts.ini')
    accounts = {}
    for section in config.sections():
        accounts[section] = {}
        for (key, val) in config.items(section):
            accounts[section][key] = val
    return accounts


def kill_hs():
    for process in psutil.process_iter():
        # check process name matches
        if process.name() == 'Hearthstone.exe':
            process.kill()


def image_path(filename):
    # shortcut 'images/' directory
    return os.path.join('images', filename)


image_logo_hs_play = image_path('hs_play_logo.png')
image_button_play = image_path('play_button.bmp')
image_button_login = image_path('green_online.png')
image_button_logout = image_path('logout.bmp')


def click_play_button():
    image_button_play = p.locateCenterOnScreen(image_button_play)
    logging.debug('button_login found at {}'.format(image_button_play))
    p.click(image_button_play[0], image_button_play[1])


def logout_from_main_window():
    config = configparser.ConfigParser()
    config.read_file(open('settings.ini'))
    battle_exe_path = config.get('settings', 'battle_net_exe_path')
    os.system(battle_exe_path)  # bring window to the front
    sleep(0.5)

    if p.locateCenterOnScreen(image_logo_hs_play) and p.locateCenterOnScreen(image_button_play):
        logging.debug('main window found')
        if p.locateCenterOnScreen(image_button_login):

            button_online_coordinates = p.locateCenterOnScreen(image_button_login)
            logging.debug('button_login found at {}'.format(button_online_coordinates))
            p.click(button_online_coordinates[0], button_online_coordinates[1])

            sleep(1)

            button_logout_coordinates = p.locateCenterOnScreen(image_button_logout)
            logging.debug('button_logout found at {}'.format(button_logout_coordinates))
            p.click(button_logout_coordinates[0], button_logout_coordinates[1])
            return True
    else:
        logging.debug('no main window')
        return False


def login(account):
    logout_from_main_window()
    account_mail = account['mail']
    account_password = account['password']

    if before_login_kill_hs:
        kill_hs()

    sleep(1)
    account_mail_split = account_mail.split('@')  # pyautogui does not want to press the @ german keyboard
    p.typewrite(account_password, interval=0.01)
    p.hotkey('shift', 'tab')
    p.typewrite(account_mail_split[0], interval=0.01)
    p.hotkey('ctrlleft', 'alt', 'q')  # @-symbol workaround
    p.typewrite(account_mail_split[1], interval=0.01)
    sleep(0.5)
    p.typewrite(['enter'], interval=0.2)
    p.typewrite(['enter'], interval=0.2)
    logging.debug('login')
    sleep(3)
    click_play_button()

# GUI


def set_kill_variable():
    global before_login_kill_hs
    if before_login_kill_hs:
        before_login_kill_hs = False
        kill_hs_button['fg'] = 'red'
        kill_hs_button['text'] = 'kill hs before login: False'
    else:
        before_login_kill_hs = True
        kill_hs_button['fg'] = 'dark green'
        kill_hs_button['text'] = 'kill hs before login: True'


def set_autostart_hs():
    global after_login_start_hs
    if after_login_start_hs:
        after_login_start_hs = False
        start_hs_button['fg'] = 'red'
        start_hs_button['text'] = 'start hs after login: False'
    else:
        after_login_start_hs = True
        start_hs_button['fg'] = 'dark green'
        start_hs_button['text'] = 'start hs after login: True'


def start_battle_net():
    config = configparser.ConfigParser()
    config.read_file(open('settings.ini'))
    battle_exe_path = config.get('settings', 'battle_net_exe_path')
    os.system(battle_exe_path)


def visual_gui_underline():
    # visual break for gui
    txt_underline = Label(root, text='_' * 32, fg='grey', bg='white')
    txt_underline.pack(padx=10, pady=1)
    return txt_underline.pack()


def initialize_accounts():
    # return all accounts
    config = configparser.ConfigParser()
    config.read('accounts.ini')
    accounts = {}
    for section in config.sections():
        accounts[section] = {}
        for (key, val) in config.items(section):
            accounts[section][key] = val
    return accounts


def create_account_buttons():
    accounts = initialize_accounts()
    for account in accounts:
        CreateButtons(accounts[account]['name'], accounts[account])


class CreateButtons:
    def __init__(self, account_name, account_number):
        self.account_name = account_name
        self.account_number = account_number
        self.button = Button(root, text=self.account_name, width=15, command=lambda: login(self.account_number))
        self.button.pack(padx=5, pady=5)
        self.button.pack()
        logging.debug('{} button created'.format(account_name))


root.wm_title('autologin v{}'.format(VERSION))
root.geometry('280x480')
root.configure(background='white')

start_battle_net_button = Button(text='start battle.net', width=15, fg='black', command=start_battle_net)
start_battle_net_button.pack(padx=5, pady=5)

kill_hs_button = Button(text='kill hs before login: True', width=30, fg='dark green', command=set_kill_variable)
kill_hs_button.pack(padx=5, pady=5)

start_hs_button = Button(text='start hs after login: True', width=30, fg='dark green', command=set_autostart_hs)
start_hs_button.pack(padx=5, pady=5)

visual_gui_underline()

txt_description = Label(root, text='accounts:', fg='grey', bg='white')
txt_description.pack(padx=10, pady=1)
txt_description.pack()

create_account_buttons()

visual_gui_underline()

txt_description = Label(root, text='misc:', fg='grey', bg='white')
txt_description.pack(padx=10, pady=1)
txt_description.pack()


def open_browser():
    git_url = 'https://github.com/JustAnotherGuy7536/autologin-battle.net'
    webbrowser.open_new_tab(git_url)


button_help = Button(root, text='help - contact', width=15, fg='black', command=open_browser)
button_help.pack(padx=5, pady=5)


def open_config():
    current_dir = os.getcwd()
    os.system(current_dir + '\\' + 'accounts.ini')


button_open_config = Button(root, text='open config file', width=15, fg='black', command=open_config)
button_open_config.pack(padx=5, pady=5)

visual_gui_underline()

txt_have_day = Label(root, text='Have a nice day.', fg='grey', bg='white')
txt_have_day.pack(padx=10, pady=1)
txt_have_day.pack()

root.resizable(width=TRUE, height=TRUE)
mainloop()
